//blue(); white(); red(); green();
$fn = 50;
layer_height = 0.2;

module card() {
    difference() {
        union() {
            polygon([[10,0], [-10,0], [-10,37], [0,47],[10,37]]);
            translate([0,51,0])
            circle(9);
        }
        translate([0,5,0])
        circle(3);
    }
}
%card();

module make_black() {
    difference() {
        linear_extrude(2)
        card();
        linear_extrude(layer_height) {
            children();
            }
    }
}

module stripe(n) {
        intersection() {
        card();
        union() {
            translate([-(n+2),0,0])
            square([2,70]);
            translate([n,0,0])
            square([2,70]);
        }
    }
}

module blue() {
    stripe(1);
}
module red() {
    stripe(3);
}
module green() {
    stripe(7);
}
module white() {
    stripe(5);
}
module black() {
    make_black() {
        blue();
        red();
        white();
        green();
    }
}
//linear_extrude(layer_height)
//blue();
//red();
//white();
//green();
black();