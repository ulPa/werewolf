//red(); blue(); white();
$fn = 50;
layer_height = 0.2;

module card() {
    hull() 
    for (i = [45:90:360]) {
        rotate([0,0,i])
        translate([25, 0, 0])
        circle(5);
    }
}
%card();

module make_black() {
    difference() {
        linear_extrude(2)
        card();
        linear_extrude(layer_height) {
            children();
            }
    }
}

module centering() {                       // makes tiny  
        for (i = [45:90:360]) {            // cylinders around
            rotate([0,0,i])                 // the object
            translate([31, 0, 0])             // so the slicer
            cylinder(r=0.3, h=layer_height); // will center it
        }
    }
centering();
module half_circle(s=5) {
    difference() {
        circle(s);
        translate([-s,0,0])
        square(s*2);
    }
}

module heart(s = 13) {
    rotate([0,0,45]) {
        square(s, center=true);
        translate([s/2,0,0])
        circle(s/2);
        translate([0,s/2,0])
        circle(s/2);
    }
}

module red() {
    translate([0,-3,0])
    heart();
}

include <bow_arrow.scad>;
module blue() {
    translate([11,15,0])
    scale(0.2)bow();
    rotate([0,0,180])
    translate([11,15,0])
    scale(0.2)bow();
}
module white() {
    rotate([0,0,90])
    translate([11,15,0])
    scale(0.2)bow();
    rotate([0,0,180+90])
    translate([11,15,0])
    scale(0.2)bow();
}

module black() {
    make_black() {
        red();
        white();
        blue();
    }
}
//linear_extrude(layer_height)
//red();
//white();
//blue();
black();