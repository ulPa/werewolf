$fn = 100;
number_of_cards = 8;
thickness_of_card = 2;
extra_card_space = 1;
card_fitting_tolerance = 1;
card_radius = 25;
card_wall_radius = 3;
floor_thickness = 2;
lid_height = 5;
lid_fitting_tolerance = 0.4;
//make the 2d shape of card
module card(s=25) {
    hull() 
    for (i = [45:90:360]) {
        rotate([0,0,i])
        translate([s, 0, 0])
        circle(5);
    }
}
//make the box
module box() {
    //make the box without lid thing
    difference() {
        linear_extrude(number_of_cards*(thickness_of_card+extra_card_space)-lid_height)
        card(card_radius+card_fitting_tolerance+card_wall_radius);
        translate([0,0,floor_thickness])
        linear_extrude(number_of_cards*(thickness_of_card+extra_card_space)-lid_height)
        card(card_radius+card_fitting_tolerance);
    }
    //make lid thing (same as before, just without -lid_height)
        difference() {
        linear_extrude(number_of_cards*(thickness_of_card+extra_card_space))
        card((card_radius+card_fitting_tolerance+card_wall_radius)-card_wall_radius/2);
        translate([0,0,floor_thickness])
        linear_extrude(number_of_cards*(thickness_of_card+extra_card_space))
        card(card_radius+card_fitting_tolerance);
    }
}
module lid() {
    difference() {
        linear_extrude(lid_height+floor_thickness)
        card(card_radius+card_fitting_tolerance+card_wall_radius);
        translate([0,0,floor_thickness])
        linear_extrude(lid_height+floor_thickness)
        card(card_radius+card_fitting_tolerance+card_wall_radius/2);
    }
}
box();
