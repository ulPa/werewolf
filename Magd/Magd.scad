//red(); white();
$fn = 50;
layer_height = 0.2;
include <magd-forms.scad>

module card() {
    hull() 
    for (i = [45:90:360]) {
        rotate([0,0,i])
        translate([25, 0, 0])
        circle(5);
    }
}
%card();
module make_black() {
    difference() {
        linear_extrude(2)
        card();
        linear_extrude(layer_height) {
            children();
            }
    }
}
module centering() {                       // makes tiny  
        for (i = [45:90:360]) {            // cylinders around
        rotate([0,0,i])                    // the object
        translate([31, 0, 0])              // so the slicer
        cylinder(r=0.5, h=layer_height); // will center it
        }
    }
centering();

module red() {
    scale([2,2])
    face();
    translate([-13,2])
    scale([1.9,1.9])
    basket();
    rotate([0,0,180])
    translate([-13,2])
    scale([1.9,1.9])
    basket();
}

module white() {
    scale([2,2])
    hat();
    translate([-14,-5])
    scale([2.2,2.2])
    milk();
    rotate([0,0,180])
    translate([-14,-5])
    scale([2.2,2.2])
    milk();
}

module black() {
    make_black() {
        red();
        white();
    }
}
//white();
//red();
black();