//red(); white(); green();
$fn = 50;
layer_height = 0.2;
include <der_alte_motives.scad>

module card() {
    hull() 
    for (i = [45:90:360]) {
        rotate([0,0,i])
        translate([25, 0, 0])
        circle(5);
    }
}
%card();
module make_black() {
    difference() {
        linear_extrude(2)
        card();
        linear_extrude(layer_height) {
            children();
            }
    }
}
module centering() {                       // makes tiny  
        for (i = [45:90:360]) {            // cylinders around
        rotate([0,0,i])                    // the object
        translate([31, 0, 0])              // so the slicer
        cylinder(r=0.5, h=layer_height); // will center it
        }
    }
centering();

module red() {
    scale(0.6)
    face();
}

module white() {
    scale(0.6)
    hair();
}

module green() {
    for (a = [0:90:360]) {
        rotate([0,0,a])
        translate([-15,10])
        scale(0.6)
        stick();
    }
}

module black() {
    make_black() {
        red();
        white();
        green();
    }
}
black();