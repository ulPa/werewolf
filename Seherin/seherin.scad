//white(); red(); green(); blue();
$fn = 50;
layer_height = 0.2;
sf = 1.4; //scale factor
module card() {
    hull() 
    for (i = [45:90:360]) {
        rotate([0,0,i])
        translate([25, 0, 0])
        circle(5);
    }
}
%card();
module centering() {                       // makes tiny  
        for (i = [45:90:360]) {            // cylinders around
            rotate([0,0,i])                 // the object
            translate([31, 0, 0])             // so the slicer
            cylinder(r=0.3, h=layer_height); // will center it
        }
    }
centering();
module eye() {
    include <seherin_auge.scad>
    translate([15,15,0])
    seer_eye();
}

module ball() {
    include <seherin_kugel.scad>
    scale(sf)seer_ball();
}


module stand() {
    include <seherin_kugel_ständer.scad>
    translate([0,-5.65*sf,0])
    scale(sf)seer_stand();
}

    

module white() {
    ball();
}

module red() {
    stand();
}

module blue() {
    for (a = [0:90*2:360]) {
        rotate([0,0,a])
        eye();
    }
}

module green() {
    for (a = [90:90*2:360]) {
        rotate([0,0,a])
        eye();
    }
}
module black() {
    difference() {
        linear_extrude(2)
        card();
        linear_extrude(layer_height) {
            green();
            blue();
            white();
            red();
        }
    }
}
    
//linear_extrude(layer_height)
//green();
//blue();
//white();
//red();
black();