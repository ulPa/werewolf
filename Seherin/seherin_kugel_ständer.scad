
// Module names are of the form poly_<inkscape-path-id>().  As a result,
// you can associate a polygon in this OpenSCAD program with the corresponding
// SVG element in the Inkscape document by looking for the XML element with
// the attribute id="inkscape-path-id".

// fudge value is used to ensure that subtracted solids are a tad taller
// in the z dimension than the polygon being subtracted from.  This helps
// keep the resulting .stl file manifold.
fudge = 0.1;

module seer_stand()
{
  scale([25.4/90, -25.4/90, 1]) union()
  {
      polygon([[-14.765507,-4.772319],[-18.426266,4.772319],[18.426266,4.772319],[14.772743,-4.753200],[11.573344,-2.188688],[7.974122,-0.303366],[4.081025,0.859661],[-0.000000,1.257287],[-4.080656,0.854459],[-7.972394,-0.313583],[-11.569312,-2.203590],[-14.765507,-4.772319]]);
  }
}


