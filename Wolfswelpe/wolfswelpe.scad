//red(); white();
$fn = 50;
layer_height = 0.2;

module card() {
    hull() 
    for (i = [45:90:360]) {
        rotate([0,0,i])
        translate([25, 0, 0])
        circle(5);
    }
}
%card();
module make_black() {
    difference() {
        linear_extrude(2)
        card();
        linear_extrude(layer_height) {
            children();
            }
    }
}
module centering() {                       // makes tiny  
        for (i = [45:90:360]) {            // cylinders around
        rotate([0,0,i])                    // the object
        translate([31, 0, 0])              // so the slicer
        cylinder(r=0.5, h=layer_height); // will center it
        }
    }
centering();
module werewolf_head() {
    translate([-6.5,-5.5,0])
    polygon([[8,0],[5,0],[4,3],[1,5],[1,8],[0,11],[2,9],[6,10],[7,10],[11,9],[13,11],[12,8],[12,5],[9,3]]);
}
module claw() {
    translate([20,0,0])
    scale([0.5,0.5,1])
    union() {
        translate([0,-4,0]) scale([16,1,1]) circle(0.5);
        translate([0,-2,0]) scale([20,1,1]) circle(0.5);
        translate([0,0,0]) scale([20,1,1]) circle(0.5);
        translate([0,2,0]) scale([18,1,1]) circle(0.5);
    }
}
module red() {
    werewolf_head();
        for (a = [45:180:360]) {
        rotate([0,0,a])
        claw();
    }
}

module white() {
        for (a = [45+90:180:360]) {
        rotate([0,0,a])
        claw();
    }
}
module black() {
    make_black() {
        red();
        white();
    }
}
//red();
//white();
black();