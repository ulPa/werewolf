//green(); red(); blue(); white();
$fn = 100;
layer_height = 0.2;
person_size = 6;
include <dorfbewohner-forms.scad>

module card() {
    hull() 
    for (i = [45:90:360]) {
        rotate([0,0,i])
        translate([25, 0, 0])
        circle(5);
    }
}
%card();
module make_black() {
    difference() {
        linear_extrude(2)
        card();
        linear_extrude(layer_height) {
            children();
            }
    }
}
module centering() {                       // makes tiny  
        for (i = [45:90:360]) {            // cylinders around
        rotate([0,0,i])                    // the object
        translate([31, 0, 0])              // so the slicer
        cylinder(r=0.5, h=layer_height); // will center it
        }
    }
centering();
    
module red() {
    translate([-2,4])
    scale([person_size,person_size])
    person_red();
}
module blue() {
    translate([-2,4])
    scale([person_size,person_size])
    difference() {
    person_blue();
        person_green();
    }
}
module green() {
    translate([-2,4])
    scale([person_size,person_size])
    person_green();

}
module white() {
    translate([12,2])
    scale([10,10])
    coin();
    translate([-14,0])
    scale([10,10])
    wheat();
    rotate([0,0,180]) {
    translate([12,2])
    scale([10,10])
    coin();
    translate([-14,0])
    scale([10,10])
    wheat();
    }
    
}
module black() {
    make_black() {
white();
green();
blue();
red();
    }
}
//linear_extrude(layer_height)
black();