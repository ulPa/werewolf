//red();
$fn = 50;
layer_height = 0.2;

module card() {
    hull() 
    for (i = [45:90:360]) {
        rotate([0,0,i])
        translate([25, 0, 0])
        circle(5);
    }
}
%card();

module bear() {
    circle(6);
    rotate([0,0,45])
    translate([-5,0,0])
    circle(3);
    rotate([0,0,-45])
    translate([-5,0,0])
    circle(3);
}
//bear();

module paw() {
    translate([15,15,0])
    for (a = [0:45:180]) {
        rotate([0,0,a])
        translate([3,0,0])
        circle(1);
    }
}

module red() {
    for (a = [0:90:360]) {
        rotate([0,0,a])
        paw();
    }
    scale(1.5) bear();
}
module black() {
    difference() {
        linear_extrude(2)
        card();
        linear_extrude(layer_height)
        red();
    }
}
//linear_extrude(layer_height)
//red();
black();