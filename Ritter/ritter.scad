//white(); blue(); red(); green();
$fn = 50;
layer_height = 0.2;

module card() {
    hull() 
    for (i = [45:90:360]) {
        rotate([0,0,i])
        translate([25, 0, 0])
        circle(5);
    }
}
%card();

module make_black() {
    difference() {
        linear_extrude(2)
        card();
        linear_extrude(layer_height) {
            children();
            }
    }
}

module centering() {                       // makes tiny  
        for (i = [45:90:360]) {            // cylinders around
            rotate([0,0,i])                 // the object
            translate([40, 0, 0])             // so the slicer
            cylinder(r=0.3, h=layer_height); // will center it
        }
    }

centering();
    
include <knight forms.scad>
    
module white() {
    translate([0,10,0])
    shield();
    rotate([0,0,180])
    translate([0,10,0])
    shield();
    rotate([0,0,180])
    translate([-6,35,0])
    helmet();
        translate([-6,35,0])
    helmet();
    translate([12, -10, 0])
    sword_handle();
}

module red() {
    translate([12,-10,0])
    sword_blade();
}

module blue() {
    translate([12, -10, 0])
    sword_blade_seperator();
}
module green() {
    translate([12, -10, 0])
    sword_diamond();
}
module black() {
    make_black() {
        green();
        blue();
        red();
        white();
    }
}
//linear_extrude(layer_height)
//green();
//blue();
//red();
//white();
black();