//blue(); red(); green();
$fn = 50;
layer_height = 0.2;
module card() {
    hull() 
    for (i = [45:90:360]) {
        rotate([0,0,i])
        translate([25, 0, 0])
        circle(5);
    }
}

module centering() {                       // makes tiny  
        for (i = [45:90:360]) {            // cylinders around
            rotate([0,0,i])                 // the object
            translate([31, 0, 0])             // so the slicer
            cylinder(r=0.3, h=layer_height); // will center it
        }
    }
centering();

//%card();
module center() {
    circle(3);
}
//center();

module corner() {
    translate([20, 0, 0]) {
        circle(4);
        translate([-5,0,0])
        square([5,3], true);
        translate([-10,5,0])
        circle(1);
        translate([-15,5,0])
        circle(2);
        
    }
}
//for (a = [45:90:360]) {
//    rotate([0,0,a])
//    corner();
//}

    
module blue() {
    center();
}
module red() {
    rotate([0,0,45])
    corner();
    rotate([0,0,180+45])
    corner();
}

module green() {
    rotate([0,0,90])
    red();
}

module black() {
    difference() {
        linear_extrude(2)
        card();
        linear_extrude(layer_height){
            blue();
            green();
            red();
        }
    }
}

black();