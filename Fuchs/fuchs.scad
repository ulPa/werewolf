//green(); white(); red();
$fn = 50;
layer_height = 0.2;

module card() {
    hull() 
    for (i = [45:90:360]) {
        rotate([0,0,i])
        translate([25, 0, 0])
        circle(5);
    }
}
%card();

module make_black() {
    difference() {
        linear_extrude(2)
        card();
        linear_extrude(layer_height) {
            children();
            }
    }
}

module centering() {                       // makes tiny  
        for (i = [45:90:360]) {            // cylinders around
            rotate([0,0,i])                 // the object
            translate([40, 0, 0])             // so the slicer
            cylinder(r=0.001, h=layer_height); // will center it
        }
    }

centering();
module red() {
    scale(2)
        difference() {
            circle(5);
            translate([5,5,0])
            circle(5);
            translate([5,-5,0])
            circle(5);
        }
}
module white() {
    scale(2) {
        intersection() {
            circle(5);
            translate([5,5,0])
            circle(5);
        }
        intersection() {
            circle(5);
            translate([5,-5,0])
            circle(5);
        }
    }
}
module leave() {
    intersection() {
        translate([-2,0,0])
        circle(4);
        translate([2,0,0])
        circle(4);
    }
    translate([0,3,0])
    square([1,5], center=true);
}

module green() {
    for (a = [0:90:360]) {
        rotate([0,0,a])
        translate([15,15,0])
        leave();
    }
}
module black() {
    make_black() {
        green();
        white();
        red();
    }
}
//linear_extrude(layer_height)
//green();
//white();
//red();
black();