//red(); green();
$fn = 50;
layer_height = 0.2;

module card() {
    hull() 
    for (i = [45:90:360]) {
        rotate([0,0,i])
        translate([25, 0, 0])
        circle(5);
    }
}

%card();
module hollow_circle(size) {
    difference() {
        circle(size);
        circle(size-1);
    }
}

module target() {
    for (s = [2:2:8]) {
        hollow_circle(s);
    }
}

module rifle() {
    
    polygon([[0,0],[0,2],[3,3],[10,3],[10,2],[3,2]]);
    translate([3,1.7,0])
    scale([0.5,0.5,1])
    hollow_circle(2);
}

module red() {
    target();
}

module green() {
    for (a = [0:90:360]) {
        rotate([0,0,a])
        translate([5,15,0])
        scale(1.5)
        rifle();
    }
}

module black() {
    difference() {
        linear_extrude(2)
        card();
        linear_extrude(layer_height) {
            green();
            red();
        }
    }
}
//linear_extrude(layer_height)
//green();
//red();
black();