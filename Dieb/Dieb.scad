//blue(); green();
$fn = 50;
layer_height = 0.2;
include <Dieb-forms.scad>

module card() {
    hull() 
    for (i = [45:90:360]) {
        rotate([0,0,i])
        translate([25, 0, 0])
        circle(5);
    }
}
%card();
module make_black() {
    difference() {
        linear_extrude(2)
        card();
        linear_extrude(layer_height) {
            children();
            }
    }
}
module centering() {                       // makes tiny  
        for (i = [45:90:360]) {            // cylinders around
        rotate([0,0,i])                    // the object
        translate([31, 0, 0])              // so the slicer
        cylinder(r=0.5, h=layer_height); // will center it
        }
    }
centering();

module blue() {
    scale(0.13)
    mask();
}

module green() {
    translate([-13,-12])
    scale(0.1)
    bag();
    rotate([0,0,180])
    translate([-13,-12])
    scale(0.1)
    bag();
}
module black() {
    make_black() {
        blue();
        green();
    }
}
//blue();
//green();
black();